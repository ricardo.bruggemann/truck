<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during execution of system
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'val.return.title' => 'Erro',
    'val.return.message' => 'Erro na validação dos dados.',
    'val.return.btnCloseText' => 'Fechar',

    'create.return.title' => 'Registro Criado',
    'create.return.message' => 'O registro foi criado com sucesso.',
    'create.return.btnCloseText' => 'Fechar',

    'update.return.title' => 'Registro Alterado',
    'update.return.message' => 'O registro foi alterado com sucesso.',
    'update.return.btnCloseText' => 'Fechar',

    'status.return.activate' => 'ativado(s)',
    'status.return.desactivate' => 'desativado(s)',
    'status.return.action' => 'Ação',
    'status.return.message' => 'Registro(s) :status_descr com sucesso!',

    'delete.return.title' => 'Registro Deletado',
    'delete.return.message' => 'Registro deletado com sucesso!',
    'delete.return.btnCloseText' => 'Fechar',

];
