<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'As senhas devem possuir ao menos 6 caracteres.',
    'reset' => 'A sua senha foi resetada!',
    'sent' => 'Nós lhe enviamos um email com instruções para recuperar a senha!',
    'token' => 'Este token para reset de senha é inválido',
    'user' => "Não encontrarmos um usuário com este email",

];
