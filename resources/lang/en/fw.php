<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Framework Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during execution of system
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'val.return.title' => 'Erro',
    'val.return.message' => 'Erro na validação dos dados.',
    'val.return.btnText' => 'Fechar',

];
