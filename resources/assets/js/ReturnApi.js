class ReturnApi {
    /**
     * Create a new Return API instance..
     */
    constructor(response, vm, padrao = true, convert_data = false, refresh = true) {
        // Irá colocar o data diretamente no response
        if(convert_data)
        {
            response = response.data;
        }

        if(response.status==401)
        {
            window.location.replace("/?status=401");
        }

        // Ajustando loading e botao
        if(padrao)
        {
            vm.loading      = false;
            vm.isDisabled   = false;
        }

        // Controla o refresh da VUETABLE
        // Padrão será sempre efetuar Refresh caso status seja 200
        if((response.vuetable || response.vuetable==undefined) && (response.status==200 || response.httpStatus==200) && refresh)
        {
            if(vm.$refs.vuetable!=undefined)
            {
                vm.$refs.vuetable.reload();
            }
        }

        // Caso não tenha o tipo da Message, sera success
        if(response.tp_message==undefined)
        {
            response.tp_message = 'success';
        }

        // Tipo 1 é Snotify
        if(response.type==1)
        {
            vm.$snotify[response.tp_message](response.message);

            // Escondendo o modal
            //vm.showModal          = false; // DEPRECATED REVISAR

            // Irá esconder todos os modais
          //for (var property in vm.$refs) {
          //    if(vm.$refs[property]!=undefined)
          //    {
          //        //var modal = vm.$refs[property];
          //        if(property.search("modal")>=0)
          //        {
          //            vm.$refs[property].hide();
          //        }
          //    }
          //}

            // vm.form.errors.clear();

        }
        // 2 é Alert
        else if(response.type==2)
        {
            // Escondendo o Modal
            vm.showModal          = false; // DEPRECATED REVISAR

            // Irá esconder todos os modais
            for (var property in vm.$refs) {
                if(vm.$refs[property]!=undefined)
                {
                    //var modal = vm.$refs[property];
                    if(property.search("modal")>=0)
                    {
                        vm.$refs[property].hide();
                    }
                }
            }

            var obj = {
              isShown: true,
              title: response.title,
              message: response.message,
              type: response.tp_message,
              customCloseBtnText: response.alert_obj.customCloseBtnText,
              customConfirmBtnText: response.alert_obj.customConfirmBtnText,
              useConfirmBtn: response.alert_obj.useConfirmBtn,
              onClose: this.onClose,
            }
            vm.$Simplert.open(obj);
        }
        // 3 Validação de formulario
        else if(response.type==3)
        {

        }
        // Nulo não apresenta nada
        else
        {

        }

        return true;
    }
}

export default ReturnApi;
