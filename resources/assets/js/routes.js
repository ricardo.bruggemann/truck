import VueRouter from 'vue-router';
import Home from './views/Home.vue';

// import Login from './components/Login.vue';

let routes = [
    { path: '/', name: 'index', component: Home, meta: { requiresAuth: true, titulo: "Home"} }
];

export default new VueRouter({
    routes,
    linkActiveClass: 'active'
});
