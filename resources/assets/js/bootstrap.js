import loadash from 'lodash';
window._ = loadash;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */
//try {
    // window.$ = window.jQuery = require('jquery');
    //require('bootstrap-sass');
//} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
 import Vue from 'vue';
 import VueRouter from 'vue-router';
 import axios from 'axios';
 import Snotify from 'vue-snotify';
 import BootstrapVue from 'bootstrap-vue';
 import VueTheMask from 'vue-the-mask';
 import money from 'v-money';
 import 'bootstrap-vue/dist/bootstrap-vue.css';
 import Multiselect from 'vue-multiselect'
 import Spinner from 'vue-simple-spinner'
 import Simplert from 'vue2-simplert-plugin';
 require('vue2-simplert-plugin/dist/vue2-simplert-plugin.css')
 import Form from './Form';
 import Errors from './Errors';
 import ReturnApi from './ReturnApi';
 import Promise from 'promise-polyfill';

if (!window.Promise) {
 	window.Promise = Promise
}
window.Form = Form;
window.Errors = Errors;
window.ReturnApi = ReturnApi;

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(Snotify);
Vue.use(Simplert);
Vue.use(Spinner);
Vue.use(Multiselect);
Vue.use(VueTheMask);
Vue.use(money);

//Vue.use(VueTour);
Vue.component('spinner', Spinner);
Vue.component('Multiselect', Multiselect);
Vue.component('Snotify', Snotify);
Vue.component('Simplert', Simplert);

window.Vue = Vue;
window.axios = axios;
window.Event = new Vue();

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */
