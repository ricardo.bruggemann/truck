
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 import  './bootstrap';
 import router from './routes';
 import VueEvents from 'vue-events';
 import Vue from 'vue'
 import Vuex from 'Vuex'

 Vue.use(Vuex);

window.Vue = require('vue');

const store = new Vuex.Store({
    state: {
        isloaded: false,
        vuetable: { filterRow : true, filterButton : false },
    },
    getters: {
        isloaded: state => state.isloaded,
    },
    mutations: {
      setVuetableFilterButton: function (state, payload) {
          state.vuetable.filterButton        = payload;
      },
      setVuetableFilterRow: function (state, payload) {
          state.vuetable.filterRow           = payload;
      },
    },
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 const app = new Vue({
     el: '#app',
     router, VueEvents,
     store: store
 });
