<!DOCTYPE html>
<html>
<head lang="en" lang="{{ app()->getLocale() }}">
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Truckpad') }}</title>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ mix('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
</head>
<body class='horizontal-layout horizontal-menu 2-columns pace-done menu-expanded'>
    <div id="app">
        <vue-snotify></vue-snotify>
        <simplert></simplert>
        <div class="mobile-menu-left-overlay"></div>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-body">
                    <router-view>
                    </router-view>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="js/vendors.min.js"></script>
    <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
</body>
</html>
