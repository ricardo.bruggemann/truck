<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Motoristas;


class CrudMotoristaTest extends TestCase
{
    /**
     * @test
     */
    public function loading_sistema()
    {
        //$employee = factory(User::class)->create();
        $this->get(route('index'))
            ->assertStatus(200)
            ->assertSee('<router-view>');

    }

    /** @test */
    public function user_can_store_motoristas()
    {
        $motoristas = [
            'nome' => 'Teste',
            'dt_nascto' => "01/01/1989",
            'sexo' => ["label"=> "Masculino", "value" => "1"],
            'tp_cnh' => ["label"=> "A", "value" => "1"],
            'veiculo_proprio' => ["label"=> "Sim", "value" => "1"],
        ];

        $this->post(route('motoristas.store'), $motoristas)
                    ->assertStatus(201)
                    ->assertJson($motoristas);

    }

    /** @test */
    public function user_can_update_motoristas()
    {
        $motoristas = [
            'nome' => 'Teste',
            'dt_nascto' => "01/01/1989",
            'sexo' => ["label"=> "Masculino", "value" => "1"],
            'tp_cnh' => ["label"=> "A", "value" => "1"],
            'veiculo_proprio' => ["label"=> "Sim", "value" => "1"],
        ];

        $motoristas_id = Motoristas::max("motorista_id");
        $this->put(route('motoristas.update', $motoristas_id), $data)
                    ->assertStatus(200)
                    ->assertJson($data);

    }

}
