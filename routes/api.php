<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/motoristas', 'HomeController@index')->name('motoristas.index');
Route::post('/motoristas/store', 'HomeController@store')->name('motoristas.store');
Route::get('/motoristas/jsonlistgeral', 'HomeController@jsonlistGeral')->name('motoristas.jsonlistgeral');
Route::post('/motoristas/update/{id}', 'HomeController@update')->name('motoristas.update');
Route::post('/motoristas/status', 'HomeController@status')->name('motoristas.status');
Route::get('/motoristas/semcarga', 'HomeController@indexSemCarga')->name('motoristas.semcarga.index');

Route::get('/viagens', 'HomeController@indexViagem')->name('viagens.index');
Route::get('/viagens/carregados', 'HomeController@indexCarregados')->name('viagens.carregados');
Route::get('/viagens/tipoveiculo', 'HomeController@indexViagensTipoVeiculo')->name('viagens.tipoveiculo');
Route::post('/viagens/store', 'HomeController@storeViagem')->name('viagens.store');



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
