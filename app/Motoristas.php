<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motoristas extends Model
{
    protected $table = 'motoristas';
    protected $primaryKey = 'motoristas_id';
    const CREATED_AT = 'dt_inc';
    const UPDATED_AT = 'dt_alt';

    /**
     * Retornado dados de status
     *
     */
    public function Sexo()
    {
        return $this->hasOne('App\ListItemValor', 'valor', 'sexo')->where('tabela','motoristas')->where('coluna', 'sexo');
    }

    /**
     * Retornado dados de status
     *
     */
    public function TpCnh()
    {
        return $this->hasOne('App\ListItemValor', 'valor', 'tp_cnh')->where('tabela','motoristas')->where('coluna', 'tp_cnh');
    }

    /**
     * Retornado dados de status
     *
     */
    public function VeiculoProprio()
    {
        return $this->hasOne('App\ListItemValor', 'valor', 'veiculo_proprio')->where('tabela','motoristas')->where('coluna', 'veiculo_proprio');
    }
}
