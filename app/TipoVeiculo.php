<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoVeiculo extends Model
{
    protected $table = 'tipo_veiculo';
    protected $primaryKey = 'tipvei_id';
    const CREATED_AT = 'dt_inc';
    const UPDATED_AT = 'dt_alt';


}
