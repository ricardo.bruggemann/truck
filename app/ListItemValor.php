<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListItemValor extends Model
{
    protected $table = 'list_item_valor';
    protected $primaryKey = 'lisiteval_id';
    const CREATED_AT = 'dt_inc';
    const UPDATED_AT = 'dt_alt';
}
