<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viagens extends Model
{
    protected $table = 'viagens';
    protected $primaryKey = 'viagen_id';
    const CREATED_AT = 'dt_inc';
    const UPDATED_AT = 'dt_alt';

    /**
    * Motorista
    */
    public function Motoristas()
    {
        return $this->hasOne('App\Motoristas', 'motoristas_id', 'motoristas_id');
    }

    /**
    * Tipo veiculo
    */
    public function TipoVeiculo()
    {
        return $this->hasOne('App\TipoVeiculo', 'tipvei_id', 'tipvei_id');
    }

    /**
     * Retornado dados de status
     *
     */
    public function EstaCarregado()
    {
        return $this->hasOne('App\ListItemValor', 'valor', 'esta_carregado')->where('tabela','viagens')->where('coluna', 'esta_carregado');
    }

    /**
     * Retornado dados de status
     *
     */
    public function TpViagem()
    {
        return $this->hasOne('App\ListItemValor', 'valor', 'tp_viagem')->where('tabela','viagens')->where('coluna', 'tp_viagem');
    }
}
