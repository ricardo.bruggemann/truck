<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Viagens;
use App\Motoristas;
use App\TipoVeiculo;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\ListItemValor;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }

    /**
    * Regras de validação de formulário
    *
    * @return Rules
    */
    public function validateRules()
    {
        return array( 'nome' => ['required', 'between:1,100' ],
                      'dt_nascto' => ['required' ],
                      'sexo.value'  => 'required',
                      'tp_cnh.value'  => 'required',
                      'veiculo_proprio.value'  => 'required',
                     );
    }

    /**
    * Regras de validação de formulário
    *
    * @return Rules
    */
    public function validateRulesViagem()
    {
        return array( 'municipio_origem' => ['required', 'between:1,100' ],
                      'municipio_destino' => ['required', 'between:1,100' ],
                      'latitude_destino' => ['required', 'between:1,100' ],
                      'longitude_destino' => ['required', 'between:1,100' ],
                      'latitude_origem' => ['required', 'between:1,100' ],
                      'longitude_origem' => ['required', 'between:1,100' ],
                      'tp_viagem.value'  => 'required',
                      'esta_carregado.value'  => 'required',
                      'tipvei.value'  => 'required',
                      'motoristas.value'  => 'required',
                     );
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $query = (new Motoristas())->newQuery();
        if($request->filled('nome')) {
            $query->where('nome', 'like', '%'.strtoupper($request->nome).'%');
        }

        if($request->filled('veiculo_proprio')) {
            $query->where('veiculo_proprio', 1);
        }

        if(empty($request->sort))
        {
            $request->sort = "nome|asc";
        }

        $order         = explode("|", $request->sort);

        $items = $query->orderBy($order[0], $order[1])
                       ->with('Sexo')
                       ->with('VeiculoProprio')
                       ->with('TpCnh')
                       ->paginate(50);

        $response = [ $items,
                      'links' => [
                            'pagination' => [
                                'total'         => $items->total(),
                                'per_page'      => $items->perPage(),
                                'current_page'  => $items->currentPage(),
                                'last_page'     => $items->lastPage(),
                                'from'          => $items->firstItem(),
                                'to'            => $items->lastItem()
                            ],
                        ]
                    ];

        return response()->json($items);
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function indexViagem(Request $request)
    {
        $query = (new Viagens())->newQuery();
        if($request->filled('nome')) {
            $query->WhereHas('Motoristas', function ($q) use ($request) {
                  $q->where('nome', 'like', '%'.strtoupper($request->nome).'%');
               }
           );
        }

        if(empty($request->sort))
        {
            $request->sort = "viagens_id|desc";
        }

        $order         = explode("|", $request->sort);

        $items = $query->orderBy($order[0], $order[1])
                       ->with('TipoVeiculo')
                       ->with('Motoristas')
                       ->with('EstaCarregado')
                       ->with('TpViagem')
                       ->paginate(50);

        $response = [ $items,
                      'links' => [
                            'pagination' => [
                                'total'         => $items->total(),
                                'per_page'      => $items->perPage(),
                                'current_page'  => $items->currentPage(),
                                'last_page'     => $items->lastPage(),
                                'from'          => $items->firstItem(),
                                'to'            => $items->lastItem()
                            ],
                        ]
                    ];

        return response()->json($items);
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function indexSemCarga(Request $request)
    {
        $query = (new Viagens())->newQuery();
        if($request->filled('nome')) {
            $query->WhereHas('Motoristas', function ($q) use ($request) {
                  $q->where('nome', 'like', '%'.strtoupper($request->nome).'%');
               }
           );
        }

        // Filtra os que estao descarregados
        $query->where('esta_carregado', 2); // Não

        if(empty($request->sort))
        {
            $request->sort = "viagens_id|desc";
        }

        $order         = explode("|", $request->sort);

        $items = $query->orderBy($order[0], $order[1])
                       ->with('TipoVeiculo')
                       ->with('Motoristas')
                       ->with('EstaCarregado')
                       ->with('TpViagem')
                       ->paginate(50);

        $response = [ $items,
                      'links' => [
                            'pagination' => [
                                'total'         => $items->total(),
                                'per_page'      => $items->perPage(),
                                'current_page'  => $items->currentPage(),
                                'last_page'     => $items->lastPage(),
                                'from'          => $items->firstItem(),
                                'to'            => $items->lastItem()
                            ],
                        ]
                    ];

        return response()->json($items);
    }

    /**
    * Display a listing of the resource. Teste
    *
    * @return \Illuminate\Http\Response
    */
    public function indexViagensTipoVeiculo(Request $request)
    {
        $items = DB::table('viagens')
                ->join('tipo_veiculo', "tipo_veiculo.tipvei_id", "viagens.tipvei_id")
                ->select(['municipio_origem', 'municipio_destino', 'tipo_veiculo.descr', DB::raw('count(*) as total')])
                ->groupBy('municipio_origem', 'municipio_destino', 'tipo_veiculo.descr')
                ->paginate(50);

        $response = [ $items,
                      'links' => [
                            'pagination' => [
                                'total'         => $items->total(),
                                'per_page'      => $items->perPage(),
                                'current_page'  => $items->currentPage(),
                                'last_page'     => $items->lastPage(),
                                'from'          => $items->firstItem(),
                                'to'            => $items->lastItem()
                            ],
                        ]
                    ];

        return response()->json($items);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->validateRules());

        if ($validator->fails())
        {
            return Response::json(array(
              'title'        => __('fw.val.return.title'),
              'message'      => __('fw.val.return.message'),
              'errors'       => $validator->getMessageBag()->toArray(),
              'type'         => 3,
              'tp_message'   => "error", // success // error // info // warning
              'alert_obj'    => [ 'customCloseBtnText' => __('fw.val.return.btnCloseText'), ]
            ), 422);
        }

        $motoristas = new Motoristas();
        $motoristas->nome             = $request->nome;
        $motoristas->dt_nascto        = Carbon::createFromFormat('d/m/Y', substr($request->dt_nascto,0,10));
        $motoristas->sexo             = $request->sexo['value'];
        $motoristas->tp_cnh           = $request->tp_cnh['value'];
        $motoristas->veiculo_proprio  = $request->veiculo_proprio['value'];
        $motoristas->save();

        return Response::json(['title'        => __('fw.create.return.title'),
              'message'      => __('fw.create.return.message'),
              'errors'       => $validator->getMessageBag()->toArray(),
              'type'         => 2,
              'tp_message'   => "success", // success // error // info // warning
              'alert_obj'    => [ 'customCloseBtnText' => __('fw.create.return.btnCloseText'), ]
          ], 201);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->validateRules());

        if ($validator->fails())
        {
            return Response::json(array(
             'title'        => __('fw.val.return.title'),
             'message'      => __('fw.val.return.message'),
             'errors'       => $validator->getMessageBag()->toArray(),
             'type'         => 3,
             'tp_message'   => "error", // success // error // info // warning
             'alert_obj'    => [ 'customCloseBtnText' => __('fw.val.return.btnCloseText'), ]
            ), 422);
        }

        $motoristas = Motoristas::findOrFail($id);
        $motoristas->nome             = $request->nome;
        $motoristas->dt_nascto        = Carbon::createFromFormat('d/m/Y', substr($request->dt_nascto,0,10));
        $motoristas->sexo             = $request->sexo['value'];
        $motoristas->tp_cnh           = $request->tp_cnh['value'];
        $motoristas->veiculo_proprio  = $request->veiculo_proprio['value'];
        $motoristas->save();

        return Response::json(['title'        => __('fw.update.return.title'),
             'message'      => __('fw.update.return.message'),
             'errors'       => $validator->getMessageBag()->toArray(),
             'type'         => 2,
             'tp_message'   => "success", // success // error // info // warning
             'alert_obj'    => [ 'customCloseBtnText' => __('fw.update.return.btnCloseText'), ]
         ], 201);

    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function storeViagem(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->validateRulesViagem());

        if ($validator->fails())
        {
            return Response::json(array(
              'title'        => __('fw.val.return.title'),
              'message'      => __('fw.val.return.message'),
              'errors'       => $validator->getMessageBag()->toArray(),
              'type'         => 3,
              'tp_message'   => "error", // success // error // info // warning
              'alert_obj'    => [ 'customCloseBtnText' => __('fw.val.return.btnCloseText'), ]
            ), 422);
        }

        $viagens = new Viagens();
        $viagens->dt_viagem             = Carbon::createFromFormat('d/m/Y', substr($request->dt_viagem,0,10));
        $viagens->tp_viagem             = $request->tp_viagem['value'];
        $viagens->esta_carregado        = $request->esta_carregado['value'];
        $viagens->municipio_origem      = $request->municipio_origem;
        $viagens->latitude_origem       = $request->latitude_origem;
        $viagens->longitude_origem      = $request->longitude_origem;
        $viagens->municipio_destino     = $request->municipio_destino;
        $viagens->latitude_destino      = $request->latitude_destino;
        $viagens->longitude_destino     = $request->longitude_destino;
        $viagens->tipvei_id             = $request->tipvei['value'];
        $viagens->motoristas_id             = $request->motoristas['value'];
        $viagens->save();

        return Response::json(['title'        => __('fw.create.return.title'),
              'message'      => __('fw.create.return.message'),
              'errors'       => $validator->getMessageBag()->toArray(),
              'type'         => 2,
              'tp_message'   => "success", // success // error // info // warning
              'alert_obj'    => [ 'customCloseBtnText' => __('fw.create.return.btnCloseText'), ]
          ], 201);
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function indexCarregados(Request $request)
    {
        $query = (new Viagens)->newQuery();

        $query->select([
               \DB::raw('(select count(*)
                            from viagens
                           where dt_viagem = curdate()
                             and esta_carregado = 1
                         ) total_dia'),
               \DB::raw('(select count(*)
                            from viagens
                           where week(dt_viagem) = week(curdate())
                             and esta_carregado = 1
                         ) total_semana'),
                \DB::raw('(select count(*)
                             from viagens
                            where month(dt_viagem) = month(curdate())
                              and esta_carregado = 1
                           ) total_mes'),
           ]);

        $items = $query->where('viagens_id', '=', Viagens::max('viagens_id'))
        ->paginate(50);

        $response = [ $items,
               'links' => [
                     'pagination' => [
                         'total'         => $items->total(),
                         'per_page'      => $items->perPage(),
                         'current_page'  => $items->currentPage(),
                         'last_page'     => $items->lastPage(),
                         'from'          => $items->firstItem(),
                         'to'            => $items->lastItem()
                     ],
                 ]
             ];

        return response()->json($items);
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function jsonList(Request $request)
    {
         $query = (new Motoristas)->newQuery();

         $items = $query->orderBy("descr", "asc")
                        ->get();

         return response()->json(compact('items'));
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function jsonlistGeral(Request $request)
    {
        // Sexo
        $query = (new ListItemValor)->newQuery();
        $query->where('tabela', "motoristas")
              ->Where('coluna', "sexo");
        $sexo = $query->orderBy("descr", "asc")
                      ->get();

        // Tipo CNH
        $query = (new ListItemValor)->newQuery();
        $query->where('tabela', "motoristas")
              ->Where('coluna', "tp_cnh");
        $tp_cnh = $query->orderBy("descr", "asc")
                      ->get();

        // Veiculo proprio
        $query = (new ListItemValor)->newQuery();
        $query->where('tabela', "motoristas")
              ->Where('coluna', "veiculo_proprio");
        $veiculo_proprio = $query->orderBy("descr", "asc")
                        ->get();

        // Indicador de carregamento
        $query = (new ListItemValor)->newQuery();
        $query->where('tabela', "viagens")
              ->Where('coluna', "esta_carregado");
        $esta_carregado = $query->orderBy("descr", "asc")
                                 ->get();

        // Tipo da viagem
        $query = (new ListItemValor)->newQuery();
        $query->where('tabela', "viagens")
              ->Where('coluna', "tp_viagem");
        $tp_viagem = $query->orderBy("descr", "asc")
                                 ->get();

        // Tipo da viagem
        $query = (new Motoristas())->newQuery();
        $motoristas = $query->orderBy("nome", "asc")
                                  ->get();

        $query = (new TipoVeiculo())->newQuery();
        $tipvei = $query->orderBy("descr", "asc")
                                ->get();

        return response()->json(compact('sexo', 'tp_cnh', 'veiculo_proprio', 'esta_carregado', 'tp_viagem', 'motoristas', 'tipvei'));
    }
}
