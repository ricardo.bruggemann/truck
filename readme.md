# Instalação

1) Sugestão - Utilizar vagrant para subir uma maquina virtual.
2) Criar o site truckpad.test
3) Adicionar a url truckpad.test ao arquivo hosts apontando para o IP do servidor ( Vagrant )
4) Criar um banco de dados
5) Na pasta raiz de sites efetuar o clone do repositório
   git clone git@gitlab.com:ricardo.bruggemann/truckpad.git

6) Renomear o arquivo .env.example para .env e edita-lo com os dados de conexão do mySQL
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=truckpad
DB_USERNAME=homestead
DB_PASSWORD=secret
```

6) Executar o comando abaixo na pasta raiz para baixar as bibliotecas do PHP
```
composer install
```

7) Conectar na VM ( vagrant ssh ), ir na pasta raiz do projeto ( /code/truckpad ) e executar o comando
```
php artisan key:generate
```

8) Executar o comando ( Cria as tabelas )
```
php artisan migrate
```

9) Executar o comando ( Roda as seeds )
```
php artisan db:seed
```

10) Acessar http://truckpad.test e validar o sistema


11) Para testar usando os testes unitários na pasta raiz do sistema
```
phpunit
```

Alguns passos são opcionais, e podem ser executados de outras maneiras.
