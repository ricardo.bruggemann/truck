<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ListItemValor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_item_valor', function (Blueprint $table) {
            $table->increments('lisiteval_id');
            $table->string('tabela', 60);
            $table->string('coluna', 60);
            $table->string('descr', 60);
            $table->string('valor', 60);
            $table->date('dt_inc');
            $table->date('dt_alt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
