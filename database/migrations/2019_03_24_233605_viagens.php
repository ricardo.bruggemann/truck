<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Viagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viagens', function (Blueprint $table) {
            $table->increments('viagens_id');
            $table->date('dt_viagem');
            $table->integer('tp_viagem'); // 1 = Carga, 2 = Descarga
            $table->integer('esta_carregado'); // 1 = Sim, 2 = Não
            $table->string('municipio_origem', 100);
            $table->decimal('latitude_origem', 10,8);
            $table->decimal('longitude_origem', 11,8);
            $table->string('municipio_destino', 100);
            $table->decimal('latitude_destino', 10,8);
            $table->decimal('longitude_destino', 11,8);
            $table->unsignedInteger('tipvei_id');
            $table->unsignedInteger('motoristas_id');
            $table->date('dt_inc');
            $table->date('dt_alt')->nullable();

            $table->index('tipvei_id', 'viag_tipveiid_idx');
            $table->foreign('tipvei_id', 'viag_tipveiid_fk')->references('tipvei_id')->on('tipo_veiculo');

            $table->index('motoristas_id', 'viag_motoristasid_idx');
            $table->foreign('motoristas_id', 'viag_motoristasid_fk')->references('motoristas_id')->on('motoristas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
