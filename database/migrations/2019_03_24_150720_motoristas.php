<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Motoristas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motoristas', function (Blueprint $table) {
            $table->increments('motoristas_id');
            $table->string('nome', 100);
            $table->date('dt_nascto');
            $table->integer('sexo');
            $table->integer('veiculo_proprio');
            $table->integer('tp_cnh');
            $table->date('dt_inc');
            $table->date('dt_alt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
