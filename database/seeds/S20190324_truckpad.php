<?php

use Illuminate\Database\Seeder;
use App\TipoVeiculo;
use App\ListItemValor;
use Carbon\Carbon;

class S20190324_truckpad extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "tp_cnh";
        $listitemvalor->descr               = "A";
        $listitemvalor->valor               = 1;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "tp_cnh";
        $listitemvalor->descr               = "B";
        $listitemvalor->valor               = 2;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "tp_cnh";
        $listitemvalor->descr               = "AB";
        $listitemvalor->valor               = 3;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "tp_cnh";
        $listitemvalor->descr               = "C";
        $listitemvalor->valor               = 4;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "tp_cnh";
        $listitemvalor->descr               = "D";
        $listitemvalor->valor               = 5;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "tp_cnh";
        $listitemvalor->descr               = "E";
        $listitemvalor->valor               = 6;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "sexo";
        $listitemvalor->descr               = "Masculino";
        $listitemvalor->valor               = 1;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "sexo";
        $listitemvalor->descr               = "Feminino";
        $listitemvalor->valor               = 2;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "veiculo_proprio";
        $listitemvalor->descr               = "Sim";
        $listitemvalor->valor               = 1;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "motoristas";
        $listitemvalor->coluna              = "veiculo_proprio";
        $listitemvalor->descr               = "Não";
        $listitemvalor->valor               = 2;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "viagens";
        $listitemvalor->coluna              = "tp_viagem";
        $listitemvalor->descr               = "Carregamento";
        $listitemvalor->valor               = 1;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "viagens";
        $listitemvalor->coluna              = "tp_viagem";
        $listitemvalor->descr               = "Descarregamento";
        $listitemvalor->valor               = 2;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "viagens";
        $listitemvalor->coluna              = "esta_carregado";
        $listitemvalor->descr               = "Sim";
        $listitemvalor->valor               = 1;
        $listitemvalor->save();

        $listitemvalor = new ListItemValor;
        $listitemvalor->tabela              = "viagens";
        $listitemvalor->coluna              = "esta_carregado";
        $listitemvalor->descr               = "Não";
        $listitemvalor->valor               = 2;
        $listitemvalor->save();

        // Tipo do veiculo
        $tipo_veiculo = new TipoVeiculo();
        $tipo_veiculo->cd = 1;
        $tipo_veiculo->descr = "Caminhão 3/4";
        $tipo_veiculo->dt_inc = Carbon::now();
        $tipo_veiculo->save();

        // Tipo do veiculo
        $tipo_veiculo = new TipoVeiculo();
        $tipo_veiculo->cd = 2;
        $tipo_veiculo->descr = "Caminhão Toco";
        $tipo_veiculo->dt_inc = Carbon::now();
        $tipo_veiculo->save();

        // Tipo do veiculo
        $tipo_veiculo = new TipoVeiculo();
        $tipo_veiculo->cd = 3;
        $tipo_veiculo->descr = "Caminhão Truck";
        $tipo_veiculo->dt_inc = Carbon::now();
        $tipo_veiculo->save();

        // Tipo do veiculo
        $tipo_veiculo = new TipoVeiculo();
        $tipo_veiculo->cd = 4;
        $tipo_veiculo->descr = "Carreta Simples";
        $tipo_veiculo->dt_inc = Carbon::now();
        $tipo_veiculo->save();

        // Tipo do veiculo
        $tipo_veiculo = new TipoVeiculo();
        $tipo_veiculo->cd = 5;
        $tipo_veiculo->descr = "Carreta Eixo Extendido";
        $tipo_veiculo->dt_inc = Carbon::now();
        $tipo_veiculo->save();
    }
}
