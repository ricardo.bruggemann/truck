const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.styles([ 'resources/assets/css/bootstrap.min.css',
               'resources/assets/css/bootstrap-extended.min.css',
               'resources/assets/css/colors.min.css',
               'resources/assets/css/components.min.css',
               'resources/assets/css/palette-gradient.min.css',
               'resources/assets/css/morris.css',
               'resources/assets/css/palette-gradient.css',
               'resources/assets/css/notify.css',
               'resources/assets/css/login.css',
               'resources/assets/css/feather.min.css',
               'resources/assets/css/flag-icon.min.css',
               'resources/assets/css/font-awesome.min.css',
               'node_modules/vue-multiselect/dist/vue-multiselect.min.css',
           ],  'public/css/main.css').version()
   .styles([ 'resources/assets/css/app.css'], 'public/css/app.css').version()
   .js('resources/assets/js/app.js', 'public/js').version();
